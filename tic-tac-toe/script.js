const koniecGry = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [6, 4, 2]
]
const AI = 'X';
const czlowiek = 'X';
let poleGry;
const pole = document.querySelectorAll('.pole');
zacznij();

function zacznij(){
    document.getElementById("h1").innerText = "";
    poleGry = Array.from(Array(9).keys());
    for(let i = 0; i < pole.length; i++){
        pole[i].innerText='';
        pole[i].style.removeProperty('background-color');
        pole[i].addEventListener('click', Klikniecie, false);
    }
    pole[4].style.color = 'red';
    pole[4].innerText = 'X';
}

let ostatniRuch;

function Klikniecie(sektor){
    if(typeof poleGry[sektor.target.id] == 'number'){
        tura(sektor.target.id, czlowiek);
        ostatniRuch = sektor.target.id;
        document.getElementById(sektor.target.id).style.color = 'green';
        if(!graTrwa()) tura(najlepszePole(), AI);
        
    }
    
}

function tura(id_sektora, gracz){
    poleGry[4] = AI;
    poleGry[id_sektora] = gracz;
    document.getElementById(id_sektora).style.color = 'red';
    document.getElementById(id_sektora).innerText = czlowiek;
    let zakonczenie = sprawdzKoniec(poleGry, gracz)
    if(zakonczenie)
        Koniec(zakonczenie)
    
}

function sprawdzKoniec(tabela, gracz){
    let zagranie = tabela.reduce((licznik, element, indeks) =>
     (element === gracz) ? licznik.concat(indeks) : licznik, []);
    let zakonczenie = null;
    for(let [index, koniec] of koniecGry.entries()){
        if(koniec.every(elementy => zagranie.indexOf(elementy) > -1)){
                zakonczenie = {index: index, gracz: gracz};
                break;
        }
        
    }
    return zakonczenie;
}

function Koniec(zakonczenie){
    for (let i = 0; i < pole.length; i++){
        pole[i].removeEventListener('click', Klikniecie, false);
    }
    document.getElementById("h1").innerText = "Przegrałeś";
    
}

function pustePola(){
    return poleGry.filter(p => typeof p == 'number');
}

function najlepszePole(){
        if(ostatniRuch == 0){
            if(poleGry[7] != 'X' && poleGry[1] != 'X'){
                return poleGry[7];
            }
            else{
                return poleGry[5];
            }
        }
    
        else if(ostatniRuch == 1){
            if(poleGry[6] != 'X' && poleGry[2] != 'X'){
                return poleGry[6];
            }
            else{
                return poleGry[8];
            }
        }
    
        else if(ostatniRuch == 2){
            if(poleGry[5] != 'X' && poleGry[3] != 'X'){
                return poleGry[3];
            }
            else{
                return poleGry[7];
            }
        }
    
        else if(ostatniRuch == 3){
            if(poleGry[8] != 'X' && poleGry[0] != 'X'){
                return poleGry[8];
            }
            else{
                return poleGry[2];
            }
        }
    
        else if(ostatniRuch == 5){
            if(poleGry[6] != 'X' && poleGry[2] != 'X'){
                return poleGry[6];
            }
            else{
                return poleGry[0];
            }
        }
        
        else if(ostatniRuch == 6){
            if(poleGry[5] != 'X' && poleGry[3] != 'X'){
                return poleGry[5];
            }
            else{
                return poleGry[1];
            }
        }
    
        else if(ostatniRuch == 7){
            if(poleGry[2] != 'X' && poleGry[6] != 'X'){
                return poleGry[2];
            }
            else{
                return poleGry[0];
            }
        }
    
        else if(ostatniRuch == 8){
            if(poleGry[1] != 'X' && poleGry[7] != 'X'){
                return poleGry[1];
            }
            else{
                return poleGry[3];
            }
        }
}

function graTrwa(){
    if(pustePola().length == 0){
        for (let i = 0; i < pole.length; i++){
            pole[i].removeEventListener('click', Klikniecie, false);
        }
    }
}
